# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone git@bitbucket.org:63160191/stroboskop.git
```

Naloga 6.2.3:
https://bitbucket.org/63160191/stroboskop/commits/131859bb47bbedded12f33c60dd7c8c951781d8e

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/63160191/stroboskop/commits/ee9ad574bbcefaa8c7883beca9199fce420f5c5b

Naloga 6.3.2:
https://bitbucket.org/63160191/stroboskop/commits/eb80d93bb812ea3a1097b4e6d5adac6a0f19a8da

Naloga 6.3.3:
https://bitbucket.org/63160191/stroboskop/commits/f23f359dcaaf9c38ee434f0daaedf55dce52fcac

Naloga 6.3.4:
https://bitbucket.org/63160191/stroboskop/commits/7f8b83b5bd9d8d97e12211a9d7b88fb368eca6b8

Naloga 6.3.5:

```
git checkout -b izgled
git checkout master
git merge izgled
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/63160191/stroboskop/commits/bc6f93c52cf7b5a49e31b1ca768eab36cadda9bd

Naloga 6.4.2:
https://bitbucket.org/63160191/stroboskop/commits/a33cdfc5507120b86e5a13d879e8a665a96b96a8

Naloga 6.4.3:
https://bitbucket.org/63160191/stroboskop/commits/efd200c656295b35b1bd41c15ea1cc9d3f5f4c57

Naloga 6.4.4:
https://bitbucket.org/63160191/stroboskop/commits/eb097a927387ecc36fe153363c662812c43f94d1